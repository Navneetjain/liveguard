package com.tagbin.user.livguard.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.tagbin.user.livguard.R;

public class VehiclePage_Activity extends AppCompatActivity {

    public ImageView bettary_bennar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_page_);
        bettary_bennar= (ImageView) findViewById(R.id.bettary_bennar);

        bettary_bennar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(VehiclePage_Activity.this,vehicle_detail_activity.class);
                startActivity(intent);
            }
        });
    }
}
