package com.tagbin.user.livguard;

import android.content.ContentValues;

/**
 * Created by user on 23-03-2017.
 */

public  abstract class AbstractEntry {

    public static final String UUID = "uuid";

    protected String uuid;

    public String getUuid() {
        return this.uuid;
    }

    public abstract ContentValues getContentValues();

    public boolean equals(AbstractEntry entity) {
        return this.getUuid().equals(entity.getUuid());
    }
}
