package com.tagbin.user.livguard.activity;

import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.tagbin.user.livguard.ProductModel;
import com.tagbin.user.livguard.R;
import com.tagbin.user.livguard.databse.DatabseBackend;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {
    private final int SPLASH_DISPLAY_LENGTH = 600;
    DatabseBackend databseBackend;
//    String mCSVfile = "livguard_spreadsheet.csv";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(MainActivity.this,VehiclePage_Activity.class);
                MainActivity.this.startActivity(mainIntent);
                MainActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);

        AssetManager manager = getAssets();
        databseBackend = DatabseBackend.getInstance(this);
        InputStream inStream=getResources().openRawResource(R.raw.livguard_spreadsheet);
        BufferedReader buffer = new BufferedReader(new InputStreamReader(inStream));
        String line = "";
        try {
            while ((line = buffer.readLine()) != null) {
                String[] colums = line.split(",");
                if (colums.length != 8) {
                    Log.d("CSVParser", "Skipping Bad CSV Row");
                    continue;
                }
                ProductModel productModel = new ProductModel(colums[0].trim(),colums[1].trim(),colums[2].trim(),colums[3].trim(),colums[4].trim(),colums[5].trim(),colums[6].trim(),colums[7].trim());
                databseBackend.createProduct(productModel);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
