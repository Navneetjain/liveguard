package com.tagbin.user.livguard.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.tagbin.user.livguard.ProductModel;
import com.tagbin.user.livguard.R;
import com.tagbin.user.livguard.databse.DatabseBackend;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class vehicle_detail_activity extends AppCompatActivity implements View.OnClickListener {
//    ImageView car,suv,auto,truck,bike,scooter;
    Spinner vehicle_spinner,model_spinner;
    private List<String> segment;
    private List<ProductModel> vehicle_model;
    LinearLayout segment_layout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_detail_activity);

        vehicle_spinner= (Spinner) findViewById(R.id.vehicle_manufacturer_spinner);
        model_spinner= (Spinner) findViewById(R.id.vehicle_model_spinner);
        segment_layout= (LinearLayout) findViewById(R.id.segment_vehicle_image);

        DatabseBackend database=DatabseBackend.getInstance(this);
        segment=new ArrayList<String>();
        segment=database.getsegment();

        for (int i=0;i<10;i++){
            final View segment_view=LayoutInflater.from(this).inflate(R.layout.segment_view,null);
            ImageView segment_image= (ImageView) findViewById(R.id.car_image);
            TextView segment_text= (TextView) findViewById(R.id.segment_text);
            segment_layout.addView(segment_view);
        }

//        vehicle=databse.getVehicleMenufacturer();
//        ArrayAdapter<ProductModel> vehicleAdapter=new ArrayAdapter<ProductModel>(this,android.R.layout.simple_spinner_item,vehicle);
//        vehicle_spinner.setAdapter(vehicleAdapter);
//
//        vehicle_model=new ArrayList<ProductModel>();
//        vehicle_model=databse.getVehicleModel();
//        ArrayAdapter<ProductModel> modelAdapter=new ArrayAdapter<ProductModel>(this,android.R.layout.simple_spinner_item,vehicle_model);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

        }
    }
}
