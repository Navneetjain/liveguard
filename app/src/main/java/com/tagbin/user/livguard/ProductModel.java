package com.tagbin.user.livguard;

import android.content.ContentValues;
import android.database.Cursor;

public class ProductModel extends AbstractEntry {
    public static final String TABLE_NAME = "battery_model ";
    public static final String SEGMENT = "segment";
    public static final String VEHICLE_MANUFACTURER = "vehicle_manufacturer";
    public static final String VEHICLE_MODEL = "vehicle_model";
    public static final String FUEL = "fuel";
    public static final String LIVGUARD_BATTERY_MODEL = "liveguard_battery_model";
    public static final String CAPACITY = "capacity";
    public static final String LIVGUARD_BRAND = "liveguard_brand";
    public static final String WARRANTY = "Warranty";

    public String vehicle_manufacture,segment,vehicle_model,fuel,liveguard_Brand,bettary_model,capacity,warranty;

    public ProductModel (String segment1,String vehicle_manufacture1,String vehicle_model1,String fuel1,String bettary_model1,String capacity1,String liveguard_Brand1,String warranty1){
        this.segment=segment1;
        this.vehicle_manufacture=vehicle_manufacture1;
        this.vehicle_model=vehicle_model1;
        this.fuel=fuel1;
        this.bettary_model=bettary_model1;
        this.capacity=capacity1;
        this.liveguard_Brand=liveguard_Brand1;
        this.warranty=warranty1;
    }

    public static String getTableName() {
        return TABLE_NAME;
    }

    public static String getVehicleManufacturer() {
        return VEHICLE_MANUFACTURER;
    }

    public static String getSEGMENT() {
        return SEGMENT;
    }

    public static String getVehicleModel() {
        return VEHICLE_MODEL;
    }

    public static String getFUEL() {
        return FUEL;
    }

    public static String getCAPACITY() {
        return CAPACITY;
    }

    public static String getLivguardBrand() {
        return LIVGUARD_BRAND;
    }

    public static String getLivguardBatteryModel() {
        return LIVGUARD_BATTERY_MODEL;
    }

    public static String getWARRANTY() {
        return WARRANTY;
    }

    public String getVehicle_manufacture() {
        return vehicle_manufacture;
    }

    public void setVehicle_manufacture(String vehicle_manufacture) {
        this.vehicle_manufacture = vehicle_manufacture;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getVehicle_model() {
        return vehicle_model;
    }

    public void setVehicle_model(String vehicle_model) {
        this.vehicle_model = vehicle_model;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getLiveguard_Brand() {
        return liveguard_Brand;
    }

    public void setLiveguard_Brand(String liveguard_Brand) {
        this.liveguard_Brand = liveguard_Brand;
    }

    public String getBettary_model() {
        return bettary_model;
    }

    public void setBettary_model(String bettary_model) {
        this.bettary_model = bettary_model;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getWarranty() {
        return warranty;
    }

    public void setWarranty(String warranty) {
        this.warranty = warranty;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values=new ContentValues();
        values.put(SEGMENT,getSegment());
        values.put(VEHICLE_MANUFACTURER,getVehicle_manufacture());
        values.put(VEHICLE_MODEL,getVehicle_model());
        values.put(FUEL,getFuel());
        values.put(LIVGUARD_BATTERY_MODEL,getLivguardBatteryModel());
        values.put(CAPACITY,getCapacity());
        values.put(LIVGUARD_BRAND,getLiveguard_Brand());
        values.put(WARRANTY,getWarranty());
        return values;
    }
    public static ProductModel fromCursor(Cursor cursor){
        return new ProductModel(cursor.getString(cursor.getColumnIndex(SEGMENT)),
                cursor.getString(cursor.getColumnIndex(VEHICLE_MANUFACTURER)),
                cursor.getString(cursor.getColumnIndex(VEHICLE_MODEL)),
                cursor.getString(cursor.getColumnIndex(FUEL)),
                cursor.getString(cursor.getColumnIndex(LIVGUARD_BATTERY_MODEL)),
                cursor.getString(cursor.getColumnIndex(CAPACITY)),
                cursor.getString(cursor.getColumnIndex(LIVGUARD_BRAND)),
                cursor.getString(cursor.getColumnIndex(WARRANTY)));
    }

}
