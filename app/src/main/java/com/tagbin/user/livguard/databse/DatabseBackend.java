package com.tagbin.user.livguard.databse;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.tagbin.user.livguard.ProductModel;
import java.util.ArrayList;

public class DatabseBackend extends SQLiteOpenHelper{
    private static DatabseBackend instance = null;
    private static final String DATABASE_NAME = "battery_products";
    private static final int DATABASE_VERSION = 29;

    public static String CREATE_PRODUCT_STATEMENT="create table "
            +ProductModel.TABLE_NAME + "("
            +ProductModel.SEGMENT + " TEXT, "
            +ProductModel.VEHICLE_MANUFACTURER + " TEXT, "
            +ProductModel.VEHICLE_MODEL + " TEXT, "
            +ProductModel.FUEL + " TEXT, "
            +ProductModel.LIVGUARD_BATTERY_MODEL + " TEXT, "
            +ProductModel.CAPACITY + " TEXT, "
            +ProductModel.LIVGUARD_BRAND + " TEXT, "
            +ProductModel.WARRANTY + " TEXT " +
            ");";


    public DatabseBackend(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

            db.execSQL(CREATE_PRODUCT_STATEMENT);
    }
    public static synchronized DatabseBackend getInstance(Context context) {
        if (instance == null) {
            instance = new DatabseBackend(context);
        }
        return instance;
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(CREATE_PRODUCT_STATEMENT);
    }
    public void createProduct(ProductModel productModel){
        SQLiteDatabase db=this.getWritableDatabase();
        db.insert(ProductModel.TABLE_NAME,null,productModel.getContentValues());
    }
    public ArrayList<String> getsegment(){
        ArrayList<String> arrayList=new ArrayList<String>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;

        cursor=db.query(ProductModel.TABLE_NAME,null,ProductModel.SEGMENT ,null,null,null,null);
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            do {

                arrayList.add(cursor.getString(cursor.getColumnIndex(ProductModel.SEGMENT)));
            }while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }
    public ArrayList<ProductModel> getVehicleMenufacturer(String segment,String vehicle_manufacturer){
        ArrayList<ProductModel> arrayList=new ArrayList<ProductModel>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        String[] arg={segment,vehicle_manufacturer};
        cursor=db.query(ProductModel.TABLE_NAME,null,ProductModel.SEGMENT
                + "=? and " + ProductModel.VEHICLE_MANUFACTURER + "=?",arg,null,null,null);
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            do {
                ProductModel model=ProductModel.fromCursor(cursor);
                arrayList.add(model);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }
    public ArrayList<ProductModel> getVehicleModel(String segment,String vehicle_manufacturer,String vehicle_model){
        ArrayList<ProductModel> arrayList=new ArrayList<ProductModel>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        String[] arg={segment,vehicle_manufacturer,vehicle_model};
        cursor=db.query(ProductModel.TABLE_NAME,null,ProductModel.SEGMENT
                + "=? and " + ProductModel.VEHICLE_MANUFACTURER + "=? and "+ ProductModel.VEHICLE_MODEL + "=?",arg,null,null,null);
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            do {
                ProductModel model=ProductModel.fromCursor(cursor);
                arrayList.add(model);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }
    public ArrayList<ProductModel> getbatteryModel(String segment,String vehicle_manufacturer,String vehicle_model,String battery_model){
        ArrayList<ProductModel> arrayList=new ArrayList<ProductModel>();
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor;
        String[] arg={segment,vehicle_manufacturer,vehicle_model,battery_model};
        cursor=db.query(ProductModel.TABLE_NAME,null,ProductModel.SEGMENT
                + "=? and " + ProductModel.VEHICLE_MANUFACTURER + "=? and "+ ProductModel.VEHICLE_MODEL + "=? and " + ProductModel.LIVGUARD_BATTERY_MODEL + "=?",arg,null,null,null);
        if (cursor.getCount()>0){
            cursor.moveToFirst();
            do {
                ProductModel model=ProductModel.fromCursor(cursor);
                arrayList.add(model);
            }while (cursor.moveToNext());
        }
        cursor.close();
        return arrayList;
    }

}
